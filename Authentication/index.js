var firebaseConfig = {
  apiKey: "",
  authDomain: "login-72509.firebaseapp.com",
  databaseURL: "https://login-72509.firebaseio.com",
  projectId: "login-72509",
  storageBucket: "login-72509.appspot.com",
  messagingSenderId: "98407255312",
  appId: "1:98407255312:web:be39ff8c1cc7da5e34a995",
  measurementId: "G-NNP4W2Z075"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth()

// Singup New User 
function register() {
  var email = document.getElementById("user_email").value;
  var pass = document.getElementById("user_pass").value;

  const promise = auth.createUserWithEmailAndPassword(email, pass);

  promise.catch(e => alert(e.message));

  alert("SignedUp Email: " + email + "Successfully");
}

// Login Existing User
function login() {
  var email = document.getElementById("user_email").value;
  var pass = document.getElementById("user_pass").value;

  const promise = auth.signInWithEmailAndPassword(email, pass);
  promise.catch(e => alert(e.message));

}

// Changing State According to user
auth.onAuthStateChanged(function (user) {
  if (user) {
    var email = user.email;
    //is signed In or SignUp	   
    alert("Active User");
  } else {
    // Not Signed In or Singup
    alert("No Active User");
  }
});